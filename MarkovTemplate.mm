fail -> fail			// infinite cycle = fail


//// CLEAN LEFT FRAME
: cleanFrame(Left, 'clean_full_frameL')
clean_full_frameL ->


//// EVAL NEXT IN A QUEUE
: goFrame(Right, '$_next_frame')
$_next_frame # -> # $
: goSigma(Right, '$')

: goFrame(Right, 'clean_full_frameL_gotoR')
clean_full_frameL_gotoR # -> clean_full_frameL

: goFull(Left, '$_first_frame')
^ $_first_frame -> ^ $


//// COPY FRAME
: dupFrame(Left, '_copyFrame', 'copyFrame_dupLeft')
: dupFrame(Right, '_copyFrame', 'copyFrame_dupRight')
: moveDuppedFrame(Right, '#', '_copyFrame')
copyFrame_dupLeft -> chk_frame_is_constR
copyFrame_dupRight # -> # #


//// CHECK IF THIS FRAME IS EQUAL TO STRING
: goSigma(Left, 'chk_frame_is_constL')
# chk_frame_is_constL -> # chk_frame_is_constR
^ chk_frame_is_constL -> ^ chk_frame_is_constR
chk_frame_is_constL ->

/// Found nonterminal
: goSigma(Right, 'chk_frame_is_constR') // skip terminals
: nextIsNonterminal(Right, 'chk_frame_is_constR')

chk_frame_is_constR -> cmp_strings // so this frame is from Sigma* and we can start compare procedure


/// COMPARE: string1 ^ ((Vn `union` Vt) #)* string2 cmp_strings #
/// Steps:
/// 0). label at string2
/// 1). dup string1
/// 2). if string2 is empty then clean and goto 3). else goto 4).
/// 3). compare string1 with Epsilon. if equal then goto 6). else goto 5).
/// 4). pop char from string2, move it to string1, compare. if equal then goto 2). else goto 5).
/// 5). compare_failure: clean the rest of string2; clean dupped string1
/// 6). compare_success: STOP MARKOV

/// 0).
: goSigma(Left, 'cmp_strings')
cmp_strings -> dup_string1_goto cmp_label

/// 1).
: goFull(Left, 'dup_string1_goto')
^ dup_string1_goto -> dup_string1 ^
: dupSigma(Left, '_copyInput', 'dup_string1')
dup_string1 -> ^
: moveDuppedFrame(Left, '^', '_copyInput')

/// 2).
cmp_label # -> cmp_string1_Eps_goto cmp_label_promise # // frame is empty
cmp_label -> cmp_by_char // frame not empty

/// 3).
: goFull(Left, 'cmp_string1_Eps_goto')
^ cmp_string1_Eps_goto -> cmp_string1_Eps ^
^ cmp_string1_Eps ^ ->. /// 6). string1 is empty
cmp_string1_Eps -> clean_string1

/// 4).
: stepFour()


/// 5). string ^ garbage clean_string1 ^ -> string ^
: goSigma(Right, 'clean_string1_goto')
clean_string1_goto -> clean_string1
clean_string1 ^ -> clean_string1
: cleanSigma(Left, 'clean_string1')
clean_string1 -> clean_frame_goto
: goFull(Right, 'clean_frame_goto')
clean_frame_goto cmp_label_promise -> clean_frame
: cleanSigma(Right, 'clean_frame')
clean_frame # ->


//// FORK FRAME
: initial_rules

$ # -> clean_full_frameL $


//// EVAL PROMISES
: promises


//// FAIL
^ $ -> fail


//// EOL
$ -> $_first_frame


//// INIT
// string -> string ^ $ S #
: goSigma(Right, '^')
: start_rule

-> ^ // empty string
