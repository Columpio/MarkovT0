import re
from typing import List
from common import *


def rules(src: str) -> ParseRule:
    if src.startswith(': '):
        return GeneratedRule(src)
    else:
        return SimpleRule(src)


def parse(code: str) -> List[ParseRule]:
    code = re.sub('\s*//.*\n', '\n', code)
    code = re.sub('\n+', '\n', code)
    code = code.strip()
    code = code.split('\n')
    return [rules(src) for src in code]


def evalMM(inp: str, rules: List[ParseRule], verbose: bool=False, debug: bool=False):
    nontrivial = \
        (lambda _: True) \
        if verbose \
        else (lambda inpu: inpu.count('_') == inpu.count('_raw'))

    inp = list(inp)
    while True:
        for rule in rules:
            ret_code, inp = rule.tryEval(inp)
            if ret_code is ReturnCode.EVAL_FAIL:
                continue
            if nontrivial(' '.join(inp)):
                print(' '.join(inp))
                if debug: input()  # read key
            if ret_code is ReturnCode.EVAL_FINISHED:
                print("\nsuccess")
                return inp
            elif ret_code is ReturnCode.EVAL_SUCCESS:
                break
        if 'fail' in inp:
            print("\nreached fail")
            return


def main():
    import sys
    with open(sys.argv[1]) as file:
        code = file.read()
    print("Parsing started")
    tree = parse(code)
    print("Parsing complete")
    print("Total Markov rules:", sum([len(x) for x in tree]))
    evalMM(input("input string: "), tree, debug=True)


if __name__ == '__main__':
    main()
