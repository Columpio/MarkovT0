import re
from enum import Enum
from typing import List, Sized

Symbol = str
GrammarSymbol = str


class ReturnCode(Enum):
    EVAL_SUCCESS = 0
    EVAL_FINISHED = 1
    EVAL_FAIL = -1


class Rule:
    def __init__(self):
        self.left = None
        self.right = None

    def getLeft(self):
        raise NotImplemented

    def getRight(self, _):
        raise NotImplemented

    def getRightSet(self):
        return {}


class AB_CD_Rule(Rule):
    def __init__(self, A: GrammarSymbol, B: GrammarSymbol, C: GrammarSymbol, D: GrammarSymbol):
        self.left = (A, B)
        self.right = (C, D)

    def getLeft(self):
        return self.left[0] + self.left[1]

    def getRight(self, maxes):
        return "%s%d %s%d" % (self.right[0], maxes[self.right[0]], self.right[1], maxes[self.right[1]])

    def getRightSet(self):
        return {self.right[0], self.right[1]}


class A_BC_Rule(Rule):
    def __init__(self, A: GrammarSymbol, B: GrammarSymbol, C: GrammarSymbol):
        self.left = A
        self.right = (B, C)

    def getLeft(self):
        return self.left

    def getRight(self, maxes):
        return "%s%d %s%d" % (self.right[0], maxes[self.right[0]], self.right[1], maxes[self.right[1]])

    def getRightSet(self):
        return {self.right[0], self.right[1]}


class A_Terminal_Rule(Rule):
    def __init__(self, A: GrammarSymbol, a: GrammarSymbol):
        self.left = A
        self.right = a

    def getLeft(self):
        return self.left

    def getRight(self, maxes):
        return "chk_frame_is_constL " + self.right


class Grammar:
    def __init__(self, start: GrammarSymbol, rules: List[str]):
        def gen(s: List[str]):
            for x in s:
                a, _, b = x.partition('->')
                a = a.strip()
                b = b.strip()
                if not b or b.islower():
                    yield A_Terminal_Rule(a, b)
                elif len(a) == len(b) == 2:
                    yield AB_CD_Rule(*(a + b))
                elif len(a) == 1 and len(b) == 2:
                    yield A_BC_Rule(*(a + b))
                else:
                    raise Exception("Bad grammar rule: %s -> %s" % (a, b))

        self.rules = list(gen([x.strip() for x in rules if x]))
        self.start = start

    @staticmethod
    def fromFile(filename: str):
        with open(filename) as file:
            t = file.read()
        t = [x for x in t.split('\n') if x]
        s = t.pop()

        return Grammar(s, t)


class RawRule:
    def __init__(self, left: List[Symbol], right: List[Symbol], isFinite: bool = False):
        self.left = left
        self.right = right
        self.isFinite = isFinite

    def __repr__(self):
        return '{} ->{} {}'.format(' '.join(self.left), '.' * self.isFinite, ' '.join(self.right))

    def tryEval(self, input: List[Symbol]):
        for i in range(len(input) - len(self.left) + 1):
            for (a, b) in zip(self.left, input[i:]):
                if a != b:
                    break
            else:
                out = input[:i] + self.right + input[i+len(self.left):]
                if self.isFinite:
                    return ReturnCode.EVAL_FINISHED, out
                else:
                    return ReturnCode.EVAL_SUCCESS, out
        return ReturnCode.EVAL_FAIL, input


class ParseRule(Sized):
    def __init__(self, src: str):
        self.src = src

    def __repr__(self):
        return "[Rule] %s" % self.src

    def tryEval(self, input: List[Symbol]):
        raise NotImplemented


class GeneratedRule(ParseRule):
    def __init__(self, src: str):
        import RuleGenerator
        super(GeneratedRule, self).__init__(src)
        self.rules = eval(src[1:].strip(), vars(RuleGenerator))

    def __len__(self):
        return len(self.rules)

    def tryEval(self, input: List[Symbol]):
        for rule in self.rules:
            ret_code, input = rule.tryEval(input)
            if ret_code is not ReturnCode.EVAL_FAIL:
                return ret_code, input
        return ReturnCode.EVAL_FAIL, input


class SimpleRule(ParseRule):
    def __init__(self, src: str):
        super(SimpleRule, self).__init__(src)
        left, right = tuple(re.split('(?:^|\s+)->\.?\s*', src))
        self.rule = RawRule(left.strip().split(), right.strip().split(), isFinite=' ->.' in src)

    def __len__(self):
        return 1

    def tryEval(self, inp: List[Symbol]):
        return self.rule.tryEval(inp)
