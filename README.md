# T0 grammar to Markov algorithm translator
## Python version used:
Python3.5+  
*Python2 and Python3.5- contributors are welcomed to fork and pull request*
## Project files:
- `Interpreter.py` - Markov interpreter
  - usage: `python Interpreter.py FileWithMarkovRules.mm`
- `common.py` - classes and functions for rule generator
- `RuleGenerator.py` - Markov rule generator functions  

## Grammar input syntax:
- T0 grammar **must** be in _Kuroda normal form_ **without** `A -> B` rules  
(which can be easily eliminated to `A -> BC; C -> ` for fresh C)
https://en.wikipedia.org/wiki/Kuroda_normal_form
- Epsilon rules are written as `A ->`
- There is an `example_grammar.txt` which you can look at
