from typing import Callable, List, Set, Dict
from common import *
from itertools import chain, groupby
from Interpreter import rules


def concat(l):
    return list(chain.from_iterable(l))


def generate_rules(grammar: Grammar):
    # 0. An     (REAL RULE), if there is no rule: A X -> ..
    # 1. ABn    (REAL RULE)
    # 2. An_raw (REAL RULE) for each A X -> ..
    # 3. An Bn -> copyFrame_dupLeft AB1$ An_raw_copyFrame Bn_copyFrame copyFrame_dupRight
    # 4. A1 -> copyFrame_dupLeft A1_raw A1_giveAway$_copyFrame copyFrame_dupRight

    def getPromises(s, postfix: str = ''):
        return [rules("{0}{1}$ -> $ {0}{1}".format(r[0].getLeft() + str(i), postfix)) for r in s for i in range(len(r))]

    def single_rule(rs: List[Rule], maxes: Dict[GrammarSymbol, int], postfix: str = '') -> List[ParseRule]:
        if rs:
            start, rest = rs[0], rs[1:]
            A = start.left

            rls = [rules(
                "$ {0}{1}{4} -> copyFrame_dupLeft {3} {0}{2}{4}$_copyFrame copyFrame_dupRight".format(A, i, i - 1,
                                                                                                      r.getRight(maxes),
                                                                                                      postfix))
                   for i, r in enumerate(rest, start=1)]

            rls.append(rules("$ {0}0{2} -> {1} $_next_frame".format(A, start.getRight(maxes), postfix)))
            return rls
        else:
            return []

    def separate_rules():
        def group(rls):
            rls.sort(key=lambda p: p.left)
            return [list(v) for _, v in groupby(rls, key=lambda p: p.left)]

        all_rules = set(grammar.rules)
        extra_letters = set(chain.from_iterable([r.getRightSet() for r in all_rules]))
        du_rules = {r for r in all_rules if type(r) == AB_CD_Rule}
        mono_rules = list(all_rules - du_rules)
        du_rules = list(du_rules)

        mono_rules = group(mono_rules)
        du_rules = group(du_rules)

        maxes = {r[0].getLeft(): len(r) - 1 for r in mono_rules + du_rules}
        AB_promises = getPromises(du_rules)
        extra_letters -= maxes.keys()
        maxes.update({l: 0 for l in extra_letters})

        return mono_rules, du_rules, maxes, extra_letters, AB_promises

    def giveAways(nms: Set[GrammarSymbol]):
        return [rules("{0}{1}_giveAway$ -> {0}{1} $".format(A, maxes[A])) for A in nms]

    def AB_rules(rs: List[AB_CD_Rule], maxes: Dict[GrammarSymbol, int]):
        # $ AB2 -> AB1 $_next_frame
        # $ AB1 -> copyFrame_dupLeft AB0$ {AB1}
        A, B = rs[0].left
        start, rest = rs[0], rs[1:]
        pat = "$ {0}{1}{2} -> copyFrame_dupLeft {4} {0}{1}{3}$_copyFrame copyFrame_dupRight"

        rls = [rules(pat.format(A, B, i, i-1, r.getRight(maxes))) for i, r in enumerate(rest, start=1)]
        rls.append(rules("$ {0}{1}0 -> {2} $_next_frame".format(A, B, start.getRight(maxes))))

        return rls

    def A_B_rules(rs: List[AB_CD_Rule], maxes: Dict[GrammarSymbol, int]):
        A, B = rs[0].left
        return rules("$ {0}{1} {2}{3} -> copyFrame_dupLeft {4}{5}$ {0}{1}_raw_copyFrame {2}{3}_copyFrame copyFrame_dupRight".format(A, maxes[A], B, maxes[B], rs[0].getLeft(), maxes[rs[0].getLeft()]))

    def A_X_rules(nms):
        return [rules("$ {0}{1} -> copyFrame_dupLeft {0}{1}_raw {0}{1}_giveAway$_copyFrame copyFrame_dupRight".format(A, maxes[A])) for A in nms]


    mono_rules, du_rules, maxes, extra_letters, promises = separate_rules()

    first_letters = {r[0].left[0] for r in du_rules}

    mmN = {l + str(maxes[l]) for l in maxes}
    mmN |= {l + str(maxes[l]) + "_raw" for l in extra_letters | first_letters}

    step = [None] * 5
    step[0] = concat([single_rule(r, maxes) for r in mono_rules if r[0].left not in first_letters])
    promises += getPromises([r for r in mono_rules if r[0].left not in first_letters])
    step[1] = concat([AB_rules(r, maxes) for r in du_rules])
    step[2] =\
        [rules("$ {0}{1}_raw -> clean_full_frameL_gotoR $_next_frame".format(l, maxes[l])) for l in extra_letters] + \
        concat([single_rule(r, maxes, '_raw') for r in mono_rules if r[0].left in first_letters])
    promises += \
        getPromises([r for r in mono_rules if r[0].left in first_letters], postfix='_raw') + \
        [rules("{0}_raw$ -> $ {0}_raw".format(l + str(maxes[l]))) for l in extra_letters]
    step[3] = [A_B_rules(r, maxes) for r in du_rules]
    step[4] = A_X_rules(first_letters)

    promises += giveAways(first_letters)

    start_rule = rules("^ -> ^ $ %s #" % (grammar.start + str(maxes[grammar.start])))

    return [x.rule for x in concat(step)], mmN, promises, [start_rule]


grammar = Grammar.fromFile("grammar.txt")

mmT = {rule.right for rule in grammar.rules if type(rule) == A_Terminal_Rule and rule.right}

initial_rules, mmN, promises, start_rule = generate_rules(grammar)


mmN_dollar = {x + '$' for x in mmN}
mmN_giveDollar = {x + '_giveAway$' for x in mmN}
mmTSharp = mmT | {"#"}
extmmN = mmN | mmN_dollar | mmN_giveDollar
extFrame = mmT | extmmN


class Move:
    def order(self, car: Symbol, tree: Symbol) -> List[Symbol]:
        raise NotImplemented


class Left(Move):
    def __repr__(self):
        return "Left"

    def order(self, car: Symbol, tree: Symbol) -> List[Symbol]:
        return [tree, car]
Left = Left()


class Right(Move):
    def __repr__(self):
        return "Right"

    def order(self, car: Symbol, tree: Symbol) -> List[Symbol]:
        return [car, tree]
Right = Right()


def goWith(st: Set[Symbol]) -> Callable[[Move, Symbol], List[RawRule]]:
    def internalgo(move: Move, symb: Symbol):
        def goMkRule(car: Symbol, tree: Symbol):
            a = move.order(car, tree)
            return RawRule(a, a[::-1])
        return [goMkRule(symb, skip_symb) for skip_symb in st]
    return internalgo


def dupWith(st: Set[Symbol]):
    def internalgo(move: Move, postfix: str, symb: Symbol):
        def goMkRule(car: Symbol, tree: Symbol):
            a = move.order(car, tree)
            b = [a[-1], tree + postfix, a[0]]
            return RawRule(a, b)
        return [goMkRule(symb, skip_symb) for skip_symb in st]
    return internalgo


def moveDuppedFrame(move: Move, border_symbol: Symbol, postfix: str):
    def goMkRule(car: Symbol, tree: Symbol):
        a = move.order(car, tree)
        return RawRule(a, a[::-1])

    def sharpMkRule(car: Symbol):
        a = move.order(car, border_symbol)
        si = a.index(border_symbol)
        b = a.copy()
        b[1-si] = b[1-si][:-len(postfix)]  # remove '
        return RawRule(a, b[::-1])
    symbs = extFrame
    return [
            goMkRule(symb + postfix, symb_const)
            for symb in symbs
            for symb_const in symbs
        ] + [sharpMkRule(symb + postfix) for symb in symbs]


def cleanWith(st: Set[Symbol]):
    def internalgo(move: Move, symb: Symbol):
        return [RawRule(move.order(symb, clean_symb), [symb]) for clean_symb in st]
    return internalgo


def nextInSet(st: Set[Symbol]):
    return lambda move, symb: [RawRule(move.order(symb, next_symb), [next_symb]) for next_symb in st]


def stepFour() -> List[RawRule]:
    a = [SimpleRule("cmp_by_char {0} -> cmp_string1_{0}_goto cmp_label_promise".format(symb)).rule for symb in mmT]
    b = concat([
            goFull(Left, 'cmp_string1_%s_goto' % symb) + [
                SimpleRule("^ cmp_string1_{0}_goto -> cmp_string1_{0}_gotoLeft ^".format(symb)).rule
            ] + goSigma(Left, 'cmp_string1_%s_gotoLeft' % symb) + [
                SimpleRule("cmp_string1_{0}_gotoLeft {0} -> cmp_success".format(symb)).rule,
                SimpleRule("cmp_string1_{0}_gotoLeft -> clean_string1_goto".format(symb)).rule
            ]
            for symb in mmT
        ])
    c = [
        SimpleRule("cmp_success ^ -> ^ cmp_success").rule
    ] + goFull(Right, 'cmp_success') + [
        SimpleRule("cmp_success cmp_label_promise -> cmp_label").rule
    ]

    return a + b + c


# Special cases
goFull = goWith(mmTSharp | mmN)
goSigma = goWith(mmT)
goFrame = goWith(mmT | mmN)

dupFrame = dupWith(extFrame)
dupSigma = dupWith(mmT)

nextIsNonterminal = nextInSet(extmmN)

cleanSigma = cleanWith(mmT)
cleanFrame = cleanWith(mmT | mmN)
